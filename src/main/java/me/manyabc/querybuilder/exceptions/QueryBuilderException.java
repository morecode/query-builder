package me.manyabc.querybuilder.exceptions;

@SuppressWarnings("serial")
public class QueryBuilderException  extends Exception {

	public QueryBuilderException(String message) {
		super(message);
	}

	public QueryBuilderException(String message, Throwable cause) {
		super(message, cause);
	}

	public QueryBuilderException(Throwable cause) {
		super(cause);
	}

	protected QueryBuilderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}

package me.manyabc.querybuilder.exceptions;

@SuppressWarnings("serial")
public class ParseError extends QueryBuilderException {

	public ParseError(String message) {
		super(message);
	}

	public ParseError(String message, Throwable cause) {
		super(message, cause);
	}

	public ParseError(Throwable cause) {
		super(cause);
	}

	protected ParseError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}

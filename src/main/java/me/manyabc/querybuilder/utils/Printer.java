package me.manyabc.querybuilder.utils;

import me.manyabc.querybuilder.client.model.FieldSuggestion;

public class Printer {

	public static String prints(FieldSuggestion fs){
		StringBuilder sb=new StringBuilder();
		sb.append(fs.getDescription()+" Supports:");
		for(String s:fs.getQueryPredicates()){
			//
			sb.append(" ["+s+"] ");
		}
		return sb.toString();
	}
}

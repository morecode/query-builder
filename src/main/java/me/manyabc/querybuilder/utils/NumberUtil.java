package me.manyabc.querybuilder.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.util.NumberUtils;

import me.manyabc.querybuilder.exceptions.ParseError;
import me.manyabc.querybuilder.exceptions.QueryBuilderException;

public class NumberUtil {



	@SuppressWarnings("unchecked")
	private static final Set<Class<?>> primitiveInt = new HashSet<Class<?>>(
			Arrays.asList(byte.class,short.class,int.class,long.class,float.class,double.class)
			);

	@SuppressWarnings("unchecked")
	private static final Set<Class<?>> nonprimitiveInt = new HashSet<Class<?>>(
			Arrays.asList(Byte.class,Short.class,Integer.class,Long.class,BigInteger.class,Float.class,Double.class,BigDecimal.class)
			);
	
	public static boolean isPrimitiveInt(Class<?> cls){
		return cls.isPrimitive() && primitiveInt.contains(cls);
	}
	

	public static boolean isNonPrimitiveInt(Class<?> cls){
		return (!cls.isPrimitive()) && ( nonprimitiveInt.contains(cls));
	}
	

	public static boolean isInt(Class<?> cls){
		return isPrimitiveInt(cls) || isNonPrimitiveInt(cls);
	}
	/**
	 * <b>supported</b> types:
	 * <ul>
	 * <li>Byte,Short,Integer,Long,BigInteger,Float,Double,BigDecimal</li>
	 * </ul>
	 * 
	 * @param type
	 * @param txt
	 * @return
	 * @throws ParseError
	 * 
	 */
	public static <N extends Number> N parseNumber(Class<N> type,String txt) throws ParseError{
		
		try {
			return NumberUtils.parseNumber(txt, type);
		} catch (IllegalArgumentException e) {
			throw new ParseError(e);
		}
	}
	
	public static Object createNumber(Class<?> type,String txt) throws QueryBuilderException{
                            
		if(type.equals(byte.class) || type.equals(Byte.class)  ){
			return parseNumber(Byte.class,txt);
		}else if(type.equals(short.class)  || type.equals(Short.class) ){
			return parseNumber(Short.class,txt);
		}else if(type.equals(int.class) || type.equals(Integer.class) ){
			return parseNumber(Integer.class,txt);
		}else if(type.equals(long.class) || type.equals(Long.class) ){
			return parseNumber(Long.class,txt);
		}else if(type.equals(float.class) || type.equals(Float.class) ){
			return parseNumber(Float.class,txt);
		}else if(type.equals(double.class) || type.equals(Double.class) ){
			return parseNumber(Double.class,txt);
		}else if(type.equals(BigInteger.class)){
			return parseNumber(BigInteger.class,txt);
		}else if(type.equals(BigDecimal.class)){
			return parseNumber(BigDecimal.class,txt);
		}
		
		throw new QueryBuilderException("type is not support :"+type.getSimpleName());
	}
	
	public static Object createNumberNoConvert(Class<?> type,String txt) throws QueryBuilderException{
        
		if(type.equals(byte.class)){
			return parseNumber(byte.class,txt);
		}else if(type.equals(short.class)){
			return parseNumber(short.class,txt);
		}else if(type.equals(int.class)){
			return parseNumber(int.class,txt);
		}else if(type.equals(long.class)){
			return parseNumber(long.class,txt);
		}else if(type.equals(float.class)){
			return parseNumber(float.class,txt);
		}else if(type.equals(double.class)){
			return parseNumber(double.class,txt);
		}else if(type.equals(Byte.class)){
			return parseNumber(Byte.class,txt);
		}else if(type.equals(Short.class)){
			return parseNumber(Short.class,txt);
		}else if(type.equals(Integer.class)){
			return parseNumber(Integer.class,txt);
		}else if(type.equals(Long.class)){
			return parseNumber(Long.class,txt);
		}else if(type.equals(Float.class)){
			return parseNumber(Float.class,txt);
		}else if(type.equals(Double.class)){
			return parseNumber(Double.class,txt);
		}else if(type.equals(BigInteger.class)){
			return parseNumber(BigInteger.class,txt);
		}else if(type.equals(BigDecimal.class)){
			return parseNumber(BigDecimal.class,txt);
		}
		
		throw new QueryBuilderException("type is not support :"+type.getSimpleName());
	}
}

package me.manyabc.querybuilder.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import me.manyabc.querybuilder.exceptions.ParseError;

public class DateTimeUtil {
	
	public static Date parseDate(String format,String txt) throws ParseError{
		try {
			return new SimpleDateFormat(format).parse(txt);
		} catch (ParseException e) {
			throw new ParseError(e);
		}
	}
}

package me.manyabc.querybuilder.utils;

import me.manyabc.querybuilder.client.model.CombinedQueryExpr;
import me.manyabc.querybuilder.client.model.SingleQueryExpr;
import me.manyabc.querybuilder.core.QDialect;
import me.manyabc.querybuilder.core.QueryConjunction;
import me.manyabc.querybuilder.core.QueryPredicate;

public class ExprUtil {
	private  final QDialect dialect;
	public ExprUtil(QDialect dialect)
	{
		this.dialect=dialect;
	}
	public  SingleQueryExpr buildSQE(String field, QueryPredicate op,String value){
		return new SingleQueryExpr(field,dialect.getQPredicateString(op),value);
	}
	
	public CombinedQueryExpr matchAll(){
		CombinedQueryExpr expr = new CombinedQueryExpr();
		expr.setRelation(dialect.getQConjunctionName(QueryConjunction.QC_AND));
		return expr;
	}

	public CombinedQueryExpr matchAny(){
		CombinedQueryExpr expr = new CombinedQueryExpr();
		expr.setRelation(dialect.getQConjunctionName(QueryConjunction.QC_OR));
		return expr;
	}
	
	public static boolean isComparePattern(QueryPredicate qpre){
		return qpre.equals(QueryPredicate.QP_EQ) ||
				qpre.equals(QueryPredicate.QP_NQ)||
				qpre.equals(QueryPredicate.QP_GT)||
				qpre.equals(QueryPredicate.QP_GTE)||
				qpre.equals(QueryPredicate.QP_LT)||
				qpre.equals(QueryPredicate.QP_LTE);
	}

	public static boolean isSearchPattern(QueryPredicate qpre){
		return qpre.equals(QueryPredicate.QP_CONTAIN);
	}
}

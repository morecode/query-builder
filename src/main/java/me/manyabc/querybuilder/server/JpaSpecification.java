package me.manyabc.querybuilder.server;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import me.manyabc.querybuilder.client.model.CombinedQueryExpr;
import me.manyabc.querybuilder.client.model.SingleQueryExpr;
import me.manyabc.querybuilder.core.QConfiger;
import me.manyabc.querybuilder.core.QueryConjunction;
import me.manyabc.querybuilder.core.QueryPredicate;
import me.manyabc.querybuilder.exceptions.ParseError;
import me.manyabc.querybuilder.exceptions.QueryBuilderException;
import me.manyabc.querybuilder.server.mapers.NumberValMaper;
import me.manyabc.querybuilder.utils.ExprUtil;
import me.manyabc.querybuilder.utils.NumberUtil;

public class JpaSpecification<T> implements Specification<T> {
	private final static String SQL_LIKE_ = "%";

	private final static Logger logger = LoggerFactory.getLogger(JpaSpecification.class);

	private final QConfiger configer;
	private final Class<T> entityClass;
	private final Collection<SingleQueryExpr> exprs;
	private QueryConjunction qc;

	public JpaSpecification(QConfiger configer, Class<T> entityClass, CombinedQueryExpr expr) {
		this.configer = configer;
		this.entityClass = entityClass;
		this.exprs = expr.getExprs();

		try {
			this.qc = getQConfiger().getQDialect().parseQConjunction(expr.getRelation());
		} catch (ParseError e) {
			this.qc = null;
		}
		logger.info(getClass().getName() + " init with configer:" + configer.getClass().getName());
	}

	public JpaSpecification(QConfiger configer, Class<T> entityClass, SingleQueryExpr expr) {
		this.configer = configer;
		this.entityClass = entityClass;
		this.exprs = Arrays.asList(expr);
		this.qc = null;
		logger.info(getClass().getName() + " init with configer:" + configer.getClass().getName());
	}

	private Field getEntityField(Class<T> entityClass, String fieldName) throws QueryBuilderException {
		try {
			return entityClass.getDeclaredField(fieldName);
		} catch (NoSuchFieldException e) {
			throw new QueryBuilderException(fieldName + "is not a field on class:" + entityClass.getSimpleName());
		} catch (SecurityException e) {
			throw new QueryBuilderException("can not get field due to SecurityException,class:"
					+ entityClass.getSimpleName() + ",filed name:" + fieldName, e);
		}
	}

	private Object readValue(Class<T> entityClass, Field field, String fieldValue, QueryPredicate qpre)
			throws QueryBuilderException {
		ValueMaper vmaper = getQConfiger().getValueMaper(entityClass, field.getName());
		if (vmaper != null) {
			return vmaper.readObjectAs(field.getType(), fieldValue);
		}
		for (ValueMaper v : getQConfiger().getBulidinValueMapers()) {
			if (v.support(field.getType(), qpre)) {
				return v.readObjectAs(field.getType(), fieldValue);
			}
		}
		throw new QueryBuilderException("can not map value to Objcet(" + field.getType().getName() + ")");
	}

	private Predicate processAsNumber(Class<T> entityClass,Field field, SingleQueryExpr expr, Root<T> root, CriteriaBuilder cb)
			throws QueryBuilderException {
		String fieldName = field.getName();
		QueryPredicate qpre = getQConfiger().getQDialect().parseQPredicate(expr.getOp());
		Expression<? extends Number> x = root.<Number>get(fieldName);
		Number y = (Number) new NumberValMaper().readObjectAs(field.getType(), expr.getPattern());

		switch (qpre) {
		case QP_EQ:
			return cb.equal(x, y);
		case QP_GT:
			return cb.gt(x, y);
		case QP_GTE:
			return cb.ge(x, y);
		case QP_LT:
			return cb.lt(x, y);
		case QP_LTE:
			return cb.le(x, y);
		case QP_NQ:
			return cb.notEqual(x, y);
		default:
			throw new IllegalArgumentException("invalid QPredicate:" + qpre);

		}
	}

	/*private Predicate processDateField(Class<T> entityClass, Field field,SingleQueryExpr expr, Root<T> root, CriteriaBuilder cb)
			throws QueryBuilderException {
		String fieldName = field.getName();
		QueryPredicate qpre = getQConfiger().getQDialect().parseQPredicate(expr.getOp());
		Expression<? extends Date> x = root.<Date>get(fieldName);
		Date y = (Date) new DateValMapper(getQConfiger().getQDialect()).readObjectAs(field.getType(), expr.getPattern());
		//Date y = (Date) readValue(entityClass, field, expr.getPattern(), qpre);
		switch (qpre) {
		case QP_EQ:
			return cb.equal(x, y);
		case QP_GT:
			return cb.greaterThan(x, y);
		case QP_GTE:
			return cb.greaterThanOrEqualTo(x, y);
		case QP_LT:
			return cb.lessThan(x, y);
		case QP_LTE:
			return cb.lessThanOrEqualTo(x, y);
		case QP_NQ:
			return cb.notEqual(x, y);
		default:
			throw new IllegalArgumentException("invalid QPredicate:" + qpre);

		}
	}
*/
	private Predicate processAsString(Class<T> entityClass, Field field,SingleQueryExpr expr, Root<T> root, CriteriaBuilder cb)
			throws QueryBuilderException {
		String fieldName = field.getName();
		
		QueryPredicate qpre = getQConfiger().getQDialect().parseQPredicate(expr.getOp());
		Expression<String> x = root.<String>get(fieldName);
		String y = expr.getPattern();
		
		switch (qpre) {
		case QP_EQ:
			return cb.equal(x, y);
		case QP_GT:
			return cb.greaterThan(x, y);
		case QP_GTE:
			return cb.greaterThanOrEqualTo(x, y);
		case QP_LT:
			return cb.lessThan(x, y);
		case QP_LTE:
			return cb.lessThanOrEqualTo(x, y);
		case QP_NQ:
			return cb.notEqual(x, y);
		case QP_CONTAIN:
			if (!y.startsWith(SQL_LIKE_)) {
				y = SQL_LIKE_ + y;
			}
			if (!y.endsWith(SQL_LIKE_)) {
				y = y + SQL_LIKE_;
			}

			return cb.like(x, y);
		default:
			throw new IllegalArgumentException("invalid QPredicate:" + qpre);

		}
	}
	

	@SuppressWarnings("unchecked")
	private Predicate processAsComparable(Class<T> entityClass, Field field,SingleQueryExpr expr, Root<T> root, CriteriaBuilder cb)
			throws QueryBuilderException {

		if(!Comparable.class.isAssignableFrom(field.getType())){
			throw new IllegalArgumentException("not a Comparable type(" + field.getType().getName() + ")");
		}
		String fieldName = field.getName();
		
		QueryPredicate qpre = getQConfiger().getQDialect().parseQPredicate(expr.getOp());
		@SuppressWarnings("rawtypes")
		Expression<? extends Comparable> x = root.get(fieldName);
		Comparable<?> y = (Comparable<?>) readValue(entityClass,field,expr.getPattern(), qpre);
		
		switch (qpre) {
		case QP_EQ:
			return cb.equal(x, y);
		case QP_GT:
			return cb.greaterThan(x, y);
		case QP_GTE:
			return cb.greaterThanOrEqualTo(x, y);
		case QP_LT:
			return cb.lessThan(x, y);
		case QP_LTE:
			return cb.lessThanOrEqualTo(x, y);
		case QP_NQ:
			return cb.notEqual(x, y);
		default:
			throw new IllegalArgumentException("QueryPredicate not support:" + qpre);

		}
	}

	private Predicate makePredicate(SingleQueryExpr expr, Class<T> entityClass, Root<T> root, CriteriaBuilder cb)
			throws QueryBuilderException {

		String fieldName = expr.getFieldName();
		Field f = getEntityField(entityClass, fieldName);
		
		QueryPredicate qpre = getQConfiger().getQDialect().parseQPredicate(expr.getOp());

		if( ExprUtil.isComparePattern(qpre) && NumberUtil.isInt(f.getType())){
			return processAsNumber(entityClass,f,expr,root,cb);
		}
		
		if( ExprUtil.isComparePattern(qpre) && Comparable.class.isAssignableFrom(f.getType())){
			return processAsComparable(entityClass,f,expr,root,cb);
		}
		if( ExprUtil.isSearchPattern(qpre) && String.class.equals(f.getType())){
			return processAsString(entityClass,f,expr,root,cb);
		}
		

		throw new QueryBuilderException("dont know how to handle ["+expr.getOp()+"] for ["+expr.getPattern() + "] on "+expr.getFieldName());
	}

	private Predicate makePredicate(Collection<SingleQueryExpr> exprs, QueryConjunction qConjunction,
			Class<T> entityClass, Root<T> root, CriteriaBuilder cb) throws QueryBuilderException {
		if (exprs == null || exprs.isEmpty()) {
			return cb.conjunction();
		}

		if (exprs.size() == 1 || qConjunction == null) {
			return makePredicate(exprs.iterator().next(), entityClass, root, cb);
		}

		Predicate p=null;
		
		if (qConjunction.equals(QueryConjunction.QC_AND)) {
			for (SingleQueryExpr qe : exprs) {
				if(p==null){
					p=cb.and(makePredicate(qe, entityClass, root, cb));
				}else{
					p=cb.and(makePredicate(qe, entityClass, root, cb),p);
				}
			}
			return p;
		} else if (qConjunction.equals(QueryConjunction.QC_OR)) {
			for (SingleQueryExpr qe : exprs) {
				if(p==null){
					p=cb.or(makePredicate(qe, entityClass, root, cb));
				}else{
					p=cb.or(makePredicate(qe, entityClass, root, cb),p);
				}
			}
			return p;
		}
		throw new IllegalArgumentException("invalid QueryConjunction:" + qConjunction);
	}

	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		try {
			return makePredicate(exprs, qc, entityClass, root, cb);
		} catch (QueryBuilderException e) {
			logger.error("toPredicate :  " + e.getMessage());
			return cb.disjunction();
		}
	}

	public QConfiger getQConfiger() {
		return configer;
	}

}

package me.manyabc.querybuilder.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import me.manyabc.querybuilder.client.model.CombinedQueryExpr;
import me.manyabc.querybuilder.client.model.SingleQueryExpr;
import me.manyabc.querybuilder.core.QConfiger;
import me.manyabc.querybuilder.core.impl.DefaultConfiger;

public class DefaultJpaQueryBuilder implements JpaQueryBuilder {
	private final static Logger logger = LoggerFactory.getLogger(DefaultJpaQueryBuilder.class);
	
	public  <T> Specification<T> buildJpaSpecification(Class<T> entityClass,CombinedQueryExpr expr)
	{
		return new JpaSpecification<T>(getQConfiger(),entityClass,expr);
	}
	
	public  <T> Specification<T> buildJpaSpecification(Class<T> entityClass,SingleQueryExpr expr)
	{
		return new JpaSpecification<T>(getQConfiger(),entityClass,expr);
	}
	
	private final QConfiger configer;

	public DefaultJpaQueryBuilder(QConfiger configer){
		this.configer=configer;
		logger.info(getClass().getName()+" init with "+configer.getClass().getName());
	}
	public DefaultJpaQueryBuilder(){
		this.configer=new DefaultConfiger();
		logger.info(getClass().getName()+" init with "+DefaultConfiger.class.getName());
	}
	

	@Override
	public QConfiger getQConfiger() {
		return configer;
	}

}

package me.manyabc.querybuilder.server;

import org.springframework.data.jpa.domain.Specification;
import me.manyabc.querybuilder.client.model.CombinedQueryExpr;
import me.manyabc.querybuilder.client.model.SingleQueryExpr;
import me.manyabc.querybuilder.core.QConfiger;


public interface JpaQueryBuilder {
	QConfiger getQConfiger();
	
	/**
	 * 创建一个查询规则
	 * @param entityClass
	 * @param expr
	 * @return
	 */
	<T> Specification<T> buildJpaSpecification(Class<T> entityClass,CombinedQueryExpr expr);
	
	/**
	 * 
	 * 创建一个查询规则
	 * @param entityClass
	 * @param expr
	 * @return 
	 */
	<T> Specification<T> buildJpaSpecification(Class<T> entityClass,SingleQueryExpr expr);

}

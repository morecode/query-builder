package me.manyabc.querybuilder.server.mapers;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import me.manyabc.querybuilder.core.QDialect;
import me.manyabc.querybuilder.core.QueryPredicate;
import me.manyabc.querybuilder.exceptions.QueryBuilderException;
import me.manyabc.querybuilder.server.ValueMaper;
import me.manyabc.querybuilder.utils.DateTimeUtil;

public class DateValMapper implements ValueMaper {

	private static final Set<QueryPredicate> queryPredicates = new HashSet<QueryPredicate>(
			Arrays.asList(QueryPredicate.QP_EQ,QueryPredicate.QP_NQ,QueryPredicate.QP_GT,QueryPredicate.QP_GTE,QueryPredicate.QP_LT,QueryPredicate.QP_LTE)
			);

	@SuppressWarnings("unchecked")
	private static final Set<Class<?>> tpyes = new HashSet<Class<?>>(
			Arrays.asList(Date.class)
			);
	
	private final QDialect dialect;
	
	public DateValMapper(QDialect dialect)
	{
		this.dialect=dialect;
	}
	@Override
	public boolean support(Class<?> clazz, QueryPredicate what) {
		return isDate(clazz) && queryPredicates.contains(what);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T>	T readObjectAs(Class<T> clazz,String val) throws QueryBuilderException {

		if(!isDate(clazz)){
			throw new IllegalArgumentException("not support this type:"+clazz.getName()); 
		}
		return (T) DateTimeUtil.parseDate(dialect.getDateTimeFmt(), val);
	}


	public static boolean isDate(Class<?> cls){
		return tpyes.contains(cls);
	}
}

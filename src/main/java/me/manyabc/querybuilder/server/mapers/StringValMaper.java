package me.manyabc.querybuilder.server.mapers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import me.manyabc.querybuilder.core.QueryPredicate;
import me.manyabc.querybuilder.exceptions.QueryBuilderException;
import me.manyabc.querybuilder.server.ValueMaper;

public class StringValMaper implements ValueMaper {

	private static final Set<QueryPredicate> queryPredicates = new HashSet<QueryPredicate>(
			Arrays.asList(QueryPredicate.QP_EQ,QueryPredicate.QP_NQ,QueryPredicate.QP_GT,QueryPredicate.QP_GTE,QueryPredicate.QP_LT,QueryPredicate.QP_LTE,QueryPredicate.QP_CONTAIN)
			);
	@Override
	public boolean support(Class<?> clazz, QueryPredicate what) {
		return queryPredicates.contains(what);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T readObjectAs(Class<T> clazz, String val) throws QueryBuilderException {
		if(!isString(clazz)){
			throw new IllegalArgumentException("not support this type:"+clazz.getName()); 
		}
		return (T) val;
	}

	public static boolean isString(Class<?> cls){
		return String.class.isAssignableFrom(cls);
	}

}

package me.manyabc.querybuilder.server.mapers;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import me.manyabc.querybuilder.core.QueryPredicate;
import me.manyabc.querybuilder.exceptions.QueryBuilderException;
import me.manyabc.querybuilder.server.ValueMaper;
import me.manyabc.querybuilder.utils.NumberUtil;

public class NumberValMaper implements ValueMaper {

	private static final Set<QueryPredicate> queryPredicates = new HashSet<QueryPredicate>(
			Arrays.asList(QueryPredicate.QP_EQ,QueryPredicate.QP_NQ,QueryPredicate.QP_GT,QueryPredicate.QP_GTE,QueryPredicate.QP_LT,QueryPredicate.QP_LTE)
			);

	@SuppressWarnings("unchecked")
	private static final Set<Class<?>> tpyes = new HashSet<Class<?>>(
			Arrays.asList(byte.class,short.class,int.class,long.class,float.class,double.class,Byte.class,Short.class,Integer.class,Long.class,BigInteger.class,Float.class,Double.class,BigDecimal.class)
			);

	@Override
	public boolean support(Class<?> clazz, QueryPredicate what) {
		return isNumber(clazz) && queryPredicates.contains(what);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T>	T readObjectAs(Class<T> clazz,String val) throws QueryBuilderException {
		if(!isNumber(clazz)){
			throw new IllegalArgumentException("not support this type:"+clazz.getName()); 
		}
		return (T) NumberUtil.createNumber(clazz, val);
	}
	

	public static boolean isNumber(Class<?> cls){
		return tpyes.contains(cls);
	}


}

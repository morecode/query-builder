package me.manyabc.querybuilder.server;

import me.manyabc.querybuilder.core.QueryPredicate;
import me.manyabc.querybuilder.exceptions.QueryBuilderException;

public interface ValueMaper {

	boolean support(Class<?> clazz,QueryPredicate what);
	
	<T>	T readObjectAs(Class<T> clazz,String val)throws QueryBuilderException;
}

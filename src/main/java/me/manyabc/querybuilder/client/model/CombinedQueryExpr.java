package me.manyabc.querybuilder.client.model;

import java.util.LinkedList;
import java.util.List;

/**
 * 分组的查询过滤<br>
 * 支持多个比较表达式,所有表达式之间的关系是单一的
 * @author john
 *
 */
public class CombinedQueryExpr {

	private List<SingleQueryExpr>	exprs = new LinkedList<SingleQueryExpr>();
	private String relation="AND";
	
	
	public CombinedQueryExpr(){
		//
	}
	public CombinedQueryExpr(SingleQueryExpr ... compExprs ){
		for(SingleQueryExpr c:compExprs){
			exprs.add(c);
		}
	}
	public List<SingleQueryExpr> getExprs() {
		return exprs;
	}
	public void setExprs(List<SingleQueryExpr> exprs) {
		this.exprs = exprs;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	
	public CombinedQueryExpr addExpr(SingleQueryExpr ... argv ){
		for(SingleQueryExpr ep:argv){
			this.exprs.add(ep);
		}
		return this;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (SingleQueryExpr ep:exprs){
			if(sb.length() > 0){
				sb.append(",");
			}
			sb.append(ep.toString());
		}
		return relation + ":"+sb.toString();
	}
	
}

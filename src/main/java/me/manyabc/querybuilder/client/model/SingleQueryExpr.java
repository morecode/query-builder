package me.manyabc.querybuilder.client.model;

public class SingleQueryExpr {

	private String fieldName;
	private String op;
	private String pattern;
	
	public SingleQueryExpr(String field,String op,String pattern){
		this.fieldName=field;
		this.op=op;
		this.pattern=pattern;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	
	@Override
	public String toString() {
		return fieldName + " "+op +" "+ pattern;
	}
	
	
}

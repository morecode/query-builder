package me.manyabc.querybuilder.client.model;

import java.util.List;

public class FieldSuggestion {
	private String name;
	private String description;
	private List<String> queryPredicates;
	
	public FieldSuggestion(){
		//
	}
	public FieldSuggestion(String name,String description,List<String> queryPredicates){
		this.name=name;
		this.description=description;
		this.queryPredicates=queryPredicates;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getQueryPredicates() {
		return queryPredicates;
	}
	public void setQueryPredicates(List<String> queryPredicates) {
		this.queryPredicates = queryPredicates;
	}
	
	
}

package me.manyabc.querybuilder.core;

import me.manyabc.querybuilder.exceptions.ParseError;

/**
 * 查询语句方言
 * @author john
 *
 */
public interface QDialect {
	
	String getName();
	
	String getQPredicateString(QueryPredicate qualifier);
	QueryPredicate parseQPredicate(String str)throws ParseError;
	
	String getQConjunctionName(QueryConjunction qc);
	QueryConjunction parseQConjunction(String val) throws ParseError ;
	
	String getDateTimeFmt();
}

package me.manyabc.querybuilder.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import me.manyabc.querybuilder.core.QueryPredicate;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface QSuggestion {
	String describtion() default "";
	boolean enabled() default true;
	QueryPredicate[] support() default {QueryPredicate.QP_EQ,QueryPredicate.QP_NQ,QueryPredicate.QP_GT,QueryPredicate.QP_GTE,QueryPredicate.QP_LT,QueryPredicate.QP_LTE};
}

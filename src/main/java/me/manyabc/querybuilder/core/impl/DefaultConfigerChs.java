package me.manyabc.querybuilder.core.impl;

import me.manyabc.querybuilder.core.QConfiger;
import me.manyabc.querybuilder.core.QDialect;

public class DefaultConfigerChs   extends AbstractConfiger  implements QConfiger{
	private static final QDialect dialect = new ChsQDialect();

	public DefaultConfigerChs(){
		super(dialect);
	}
}

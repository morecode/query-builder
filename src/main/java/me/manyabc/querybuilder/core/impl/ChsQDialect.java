package me.manyabc.querybuilder.core.impl;

import me.manyabc.querybuilder.core.QDialect;
import me.manyabc.querybuilder.core.QueryConjunction;
import me.manyabc.querybuilder.core.QueryPredicate;
import me.manyabc.querybuilder.exceptions.ParseError;

public class ChsQDialect  implements QDialect{
	public static final String OP_EQ="是";
	public static final String OP_NQ="不是";
	public static final String OP_GT="大于";
	public static final String OP_GTE="大于或者等于";
	public static final String OP_LT="小于";
	public static final String OP_LTE="小于或者等于";
	public static final String OP_LIKE="包含";
	
	public static final String AND="满足所有条件";
	public static final String OR="满足任意一个条件";

	public static final String DATE_STR_FORMAT="yyyy-MM-dd HH:mm:ss";
	@Override
	public String getQPredicateString(QueryPredicate qpre) {
		switch (qpre) {
		case QP_CONTAIN:
			return OP_LIKE;
		case QP_EQ:
			return OP_EQ;
		case QP_GT:
			return OP_GT;
		case QP_GTE:
			return OP_GTE;
		case QP_LT:
			return OP_LT;
		case QP_LTE:
			return OP_LTE;
		case QP_NQ:
			return OP_NQ;
		default:
			// someone add new value for QueryPredicate ?
			throw new IllegalArgumentException("invalid QueryPredicate:"+qpre);
		}
	}

	@Override
	public QueryPredicate parseQPredicate(String val) throws ParseError {
		String str=val.trim();
		if(str.equals(OP_EQ)){
			return QueryPredicate.QP_EQ;
		}else if(str.equals(OP_NQ)){
			return QueryPredicate.QP_NQ;
		}else if(str.equals(OP_GT)){
			return QueryPredicate.QP_GT;
		}else if(str.equals(OP_GTE)){
			return QueryPredicate.QP_GTE;
		}else if(str.equals(OP_LT)){
			return QueryPredicate.QP_LT;
		}else if(str.equals(OP_LTE)){
			return QueryPredicate.QP_LTE;
		}else if(str.equals(OP_LIKE)){
			return QueryPredicate.QP_CONTAIN;
		}
		
		throw new ParseError("Unknown QueryPredicate name:"+val);
	}


	@Override
	public String getQConjunctionName(QueryConjunction qc) {
		switch (qc) {
		case QC_AND:
			return AND;
		case QC_OR:
			return OR;
		default:
			// someone add new value for QueryPredicate ?
			throw new IllegalArgumentException("invalid QueryConjunction:"+qc);
		}
	}

	@Override
	public QueryConjunction parseQConjunction(String val) throws ParseError {
		String str=val.trim();
		if(str.equals(AND)){
			return QueryConjunction.QC_AND;
		}else if(str.equals(OR)){
			return QueryConjunction.QC_OR;
		}
		
		throw new ParseError("Unknown QueryConjunction name:"+val);
	}

	@Override
	public String getDateTimeFmt() {
		return DATE_STR_FORMAT;
	}

	@Override
	public String getName() {
		return "默认查询方言(中文)";
	}
}

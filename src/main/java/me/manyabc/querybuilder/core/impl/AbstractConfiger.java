package me.manyabc.querybuilder.core.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.manyabc.querybuilder.core.QConfiger;
import me.manyabc.querybuilder.core.QDialect;
import me.manyabc.querybuilder.server.ValueMaper;
import me.manyabc.querybuilder.server.mapers.DateValMapper;
import me.manyabc.querybuilder.server.mapers.NumberValMaper;
import me.manyabc.querybuilder.server.mapers.StringValMaper;

public class AbstractConfiger implements QConfiger {

	private final QDialect dialect;
	private final List<ValueMaper> buildinMapers;

	private final Map<String, ValueMaper> regedValueMaper = new HashMap<String, ValueMaper>();

	public AbstractConfiger(QDialect dialect) {
		this.dialect = dialect;
		this.buildinMapers = Arrays.asList(new DateValMapper(dialect), new NumberValMaper(), new StringValMaper());
	}

	@Override
	public QDialect getQDialect() {
		return dialect;
	}

	protected String makeKey(Class<?> entityClass, String fieldName) {
		return entityClass.getName() + "@" + fieldName;
	}

	@Override
	public List<ValueMaper> getBulidinValueMapers() {
		return buildinMapers;
	}

	@Override
	public ValueMaper getValueMaper(Class<?> entityClass, String fieldName) {
		String k = makeKey(entityClass, fieldName);
		if (regedValueMaper.containsKey(k)) {
			return regedValueMaper.get(k);
		}
		return null;
	}

	@Override
	public void addValueMaper(Class<?> entityClass, String fieldName, ValueMaper maper) {
		regedValueMaper.put(makeKey(entityClass, fieldName), maper);

	}

}

package me.manyabc.querybuilder.core.impl;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import me.manyabc.querybuilder.client.model.FieldSuggestion;
import me.manyabc.querybuilder.core.QConfiger;
import me.manyabc.querybuilder.core.QueryPredicate;
import me.manyabc.querybuilder.core.annotations.QSuggestion;
import me.manyabc.querybuilder.core.QSuggestionGenerater;
import me.manyabc.querybuilder.exceptions.QueryBuilderException;
import me.manyabc.querybuilder.server.mapers.DateValMapper;
import me.manyabc.querybuilder.server.mapers.NumberValMaper;
import me.manyabc.querybuilder.server.mapers.StringValMaper;

public class DefaultSuggestionGenerater implements QSuggestionGenerater {

	private final static Logger logger = LoggerFactory.getLogger(DefaultSuggestionGenerater.class);
	
	

	public final static QueryPredicate[] QueryPredicateForDate={
			QueryPredicate.QP_EQ,
			QueryPredicate.QP_NQ,
			QueryPredicate.QP_GT,
			QueryPredicate.QP_GTE,
			QueryPredicate.QP_LT,
			QueryPredicate.QP_LTE
			}; 
	public final static QueryPredicate[] QueryPredicateForString={
			QueryPredicate.QP_EQ,
			QueryPredicate.QP_NQ,
			QueryPredicate.QP_GT,
			QueryPredicate.QP_GTE,
			QueryPredicate.QP_LT,
			QueryPredicate.QP_LTE,
			QueryPredicate.QP_CONTAIN
			}; 
	public final static QueryPredicate[] QueryPredicateForNumber={
			QueryPredicate.QP_EQ,
			QueryPredicate.QP_NQ,
			QueryPredicate.QP_GT,
			QueryPredicate.QP_GTE,
			QueryPredicate.QP_LT,
			QueryPredicate.QP_LTE
			}; 
	private final QConfiger configer;
	
	public DefaultSuggestionGenerater(QConfiger configer){
		this.configer=configer;
	}
	
	private List<String> getQueryPredicateString(List<QueryPredicate> qps){
		List<String> lst = new LinkedList<String>();
		for(QueryPredicate qp: qps){
			lst.add(configer.getQDialect().getQPredicateString(qp));
		}
		return lst;
	}
	
	/**
	 * 
	 * @param field
	 * @param loadTo
	 * @return true :  annotation is presented 
	 */
	private boolean loadFromQAnnotation(Field field,List<FieldSuggestion> loadTo){
		QSuggestion qAnnotation=field.getAnnotation(QSuggestion.class);
		if(qAnnotation == null ){
			return false;
		}
		if(qAnnotation.enabled()){
			String describtion=qAnnotation.describtion();
			if(StringUtils.isBlank(describtion)){
				describtion=field.getName();
			}
			FieldSuggestion fs= new FieldSuggestion( field.getName(),describtion ,getQueryPredicateString(Arrays.asList(qAnnotation.support())));
			loadTo.add(fs);
		}
		return true;
	}
	@Override
	public List<FieldSuggestion> getFieldSuggestions(Class<?> cls) throws QueryBuilderException {
		List<FieldSuggestion> list = new LinkedList<FieldSuggestion>();
		try {
			Field[] fds = cls.getDeclaredFields();
			for(Field f:fds){
				logger.debug("try process "+cls.getSimpleName()+"."+f.getName()+",type="+ f.getType().getSimpleName());
				if(loadFromQAnnotation(f,list) ){
					continue;
				}
				if(DateValMapper.isDate(f.getType())){
					list.add(new FieldSuggestion(f.getName(),f.getName(),getQueryPredicateString(Arrays.asList(QueryPredicateForDate))));
					
				}else if(NumberValMaper.isNumber(f.getType())){
					list.add(new FieldSuggestion(f.getName(),f.getName(),getQueryPredicateString(Arrays.asList(QueryPredicateForNumber))));
					
				}else if(StringValMaper.isString(f.getType())){
					list.add(new FieldSuggestion(f.getName(),f.getName(),getQueryPredicateString(Arrays.asList(QueryPredicateForString))));
					
				}else{

					logger.debug("ignored => "+cls.getSimpleName()+"."+f.getName()+",type="+ f.getType().getSimpleName());
				}
			}
			
			return list;
		}catch (SecurityException e) {
			throw new QueryBuilderException("can not get field due to SecurityException,class:"+cls.getSimpleName(),e);
		}
	}
}

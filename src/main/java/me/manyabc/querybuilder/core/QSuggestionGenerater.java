package me.manyabc.querybuilder.core;

import java.util.List;

import me.manyabc.querybuilder.client.model.FieldSuggestion;
import me.manyabc.querybuilder.exceptions.QueryBuilderException;

public interface QSuggestionGenerater {
	List<FieldSuggestion>	getFieldSuggestions(Class<?> cls) throws QueryBuilderException;
}

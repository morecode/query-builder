package me.manyabc.querybuilder.core;

/**
 * 查询谓语,用于查询表达式内部{@link SingleQueryExpr}
 * @author john
 *
 */
public enum QueryPredicate {

	QP_EQ,		/*  等于  */
	QP_NQ,		/*  不等于  */
	QP_GT,		/*  大于  */
	QP_GTE,		/*  大于或者等于  */
	QP_LT,		/*  小于  */
	QP_LTE,		/*  小于或者等于  */
	QP_CONTAIN	/*  包含  */
}

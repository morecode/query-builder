package me.manyabc.querybuilder.core;

import java.util.List;

import me.manyabc.querybuilder.server.ValueMaper;

public interface QConfiger {

	QDialect getQDialect();

	/**
	 * 获取一个ValueMaper(根据实体类型和该类型的字段名称)<br>
	 * @param entityClass
	 * @param fieldName
	 * @return 如果该ValueMaper未注册返回null
	 */
	ValueMaper		getValueMaper(Class<?> entityClass,String fieldName);
	

	/**
	 * 注册ValueMaper
	 * @param entityClass
	 * @param fieldName
	 */
	void			addValueMaper(Class<?> entityClass,String fieldName,ValueMaper maper);
	
	/**
	 * 获取内建ValueMaper
	 * @return 如果
	 */
	List<ValueMaper>		getBulidinValueMapers();
}

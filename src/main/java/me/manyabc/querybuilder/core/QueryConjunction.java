package me.manyabc.querybuilder.core;

/**
 * 查询连接符,用于连接查询表达式{@link SingleQueryExpr}
 * @author john
 *
 */
public enum QueryConjunction {

	QC_AND,
	QC_OR
}

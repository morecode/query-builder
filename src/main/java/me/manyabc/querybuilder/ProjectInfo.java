package me.manyabc.querybuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 当前项目的版本号 ,来自project.properties
 * @author john
 *
 */
public class ProjectInfo {

	public static final String version;
	public static final String UNKNOWN_VERSION="unknow";

	static {
		Properties properties = new Properties();
		String ver=UNKNOWN_VERSION;
		try {
			InputStream stream = ProjectInfo.class.getResourceAsStream("/project.properties");
			properties.load(stream);
			stream.close();
			ver = properties.getProperty("version");
		} catch (IOException e) {
			e.printStackTrace();
		}
		version = ver;
	}
	
}

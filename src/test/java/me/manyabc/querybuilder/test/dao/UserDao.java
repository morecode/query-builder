package me.manyabc.querybuilder.test.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import me.manyabc.querybuilder.test.entity.User;


public interface UserDao extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

}

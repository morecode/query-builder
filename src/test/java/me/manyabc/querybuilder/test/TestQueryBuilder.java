package me.manyabc.querybuilder.test;

import me.manyabc.querybuilder.client.model.CombinedQueryExpr;
import me.manyabc.querybuilder.client.model.SingleQueryExpr;
import me.manyabc.querybuilder.core.QConfiger;
import me.manyabc.querybuilder.core.QueryPredicate;
import me.manyabc.querybuilder.server.DefaultJpaQueryBuilder;
import me.manyabc.querybuilder.server.JpaQueryBuilder;
import me.manyabc.querybuilder.test.config.JpaTestConfig;
import me.manyabc.querybuilder.test.config.TestConfig;
import me.manyabc.querybuilder.test.dao.UserDao;
import me.manyabc.querybuilder.test.entity.User;
import me.manyabc.querybuilder.utils.ExprUtil;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JpaTestConfig.class, TestConfig.class })
public class TestQueryBuilder {
	@Autowired
	UserDao userDao;
	
	@Autowired
	@Qualifier("qconfiger")
	QConfiger qConfiger;
	
	private static final String preCreatedUser1="testUser1";
	private static final String preCreatedUser2="testUser2";
	private static final String preCreatedUser3="testUser3";

	public static User createTestUser(String userName,int cardNumber) {
		return createTestUser(userName,(userName+"@exapmle.com"),cardNumber);
	}
	public static User createTestUser(String userName,String email,int cardNumber) {

		User user = new User();
		user.setAddress("");
		user.setCardNumber(cardNumber);
		user.setEmail(email);
		user.setName(userName);
		user.setPassword("1234");
		user.setPhone("110");
		user.setType("test");
		return user;
	}

	private void addTestData() {
		
		User user = userDao.save(createTestUser(preCreatedUser1,100));
		assertNotNull(user);
		user = userDao.save(createTestUser(preCreatedUser2,200));
		assertNotNull(user);
		user = userDao.save(createTestUser(preCreatedUser3,300));
		assertNotNull(user); 
		
	}

	public void deleteTestData()  {
		userDao.deleteAll();
		System.out.println("test done.");
	}


	@Test
	public void testSingleQry() {
		addTestData();
		JpaQueryBuilder qbuilder=new DefaultJpaQueryBuilder(qConfiger);
		ExprUtil exprUtil=new ExprUtil(qbuilder.getQConfiger().getQDialect());
	
		SingleQueryExpr expr=null;
		Long count=0L; 
		
		expr=exprUtil.buildSQE ("name",QueryPredicate.QP_EQ,preCreatedUser1);
		count=userDao.count( qbuilder.buildJpaSpecification(User.class, expr) );
		System.out.println("query for:"+expr.toString()+" => "+ count);
		assertEquals(true ,count == 1L); 
		

		expr=exprUtil.buildSQE ("name",QueryPredicate.QP_CONTAIN,"test");
		count=userDao.count( qbuilder.buildJpaSpecification(User.class, expr) );
		System.out.println("query for:"+expr.toString()+" => "+ count);
		assertEquals(true ,count == 3L); 
		

		expr=exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_GT,"100");
		count=userDao.count(qbuilder.buildJpaSpecification(User.class,expr ));
		System.out.println("query for:"+expr.toString()+" => "+ count);
		assertEquals(true ,count == 2L);
		

		expr=exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_GTE,"100");
		count=userDao.count(qbuilder.buildJpaSpecification(User.class, expr));
		System.out.println("query for:"+expr.toString()+" => "+ count);
		assertEquals(true ,count == 3L);
		

		expr= exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_LT,"100");
		count=userDao.count(qbuilder.buildJpaSpecification(User.class,expr));
		System.out.println("query for:"+expr.toString()+" => "+ count);
		assertEquals(true ,count == 0L);
		

		expr=exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_LTE,"100");
		count=userDao.count(qbuilder.buildJpaSpecification(User.class,expr ));
		System.out.println("query for:"+expr.toString()+" => "+ count);
		assertEquals(true ,count == 1L);

		deleteTestData();
	}
	@Test
	public void testMatchAll() {
		addTestData();
		JpaQueryBuilder qbuilder=new DefaultJpaQueryBuilder(qConfiger);
		ExprUtil exprUtil=new ExprUtil(qbuilder.getQConfiger().getQDialect());
	
		Long count=0L;

		CombinedQueryExpr expr = exprUtil.matchAll();
		expr
		.addExpr(exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_GT,"100"))
		.addExpr(exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_LT,"300"));
		
		count=userDao.count(qbuilder.buildJpaSpecification(User.class, expr));
		System.out.println("query for:"+expr.toString()+" => "+ count);
		assertEquals(true ,count == 1L);
		
		
		expr = exprUtil.matchAll();
		expr
		.addExpr(exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_GTE,"100"))
		.addExpr(exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_LTE,"300"))
		.addExpr(exprUtil.buildSQE ("name",QueryPredicate.QP_EQ,preCreatedUser1));
		
		count=userDao.count(qbuilder.buildJpaSpecification(User.class, expr));
		System.out.println("query for:"+expr.toString()+" => "+ count);
		assertEquals(true ,count == 1L);
		
		
		expr = exprUtil.matchAll();
		expr
		.addExpr(exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_GTE,"100"))
		.addExpr(exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_LTE,"300"))
		.addExpr(exprUtil.buildSQE ("name",QueryPredicate.QP_EQ,"xxx"));
		
		count=userDao.count(qbuilder.buildJpaSpecification(User.class, expr));
		System.out.println("query for:"+expr.toString()+" => "+ count);
		assertEquals(true ,count == 0L);
		
		deleteTestData();

	}
	
	@Test
	public void testMatchAny() {
		addTestData();
		JpaQueryBuilder qbuilder=new DefaultJpaQueryBuilder(qConfiger);
		ExprUtil exprUtil=new ExprUtil(qbuilder.getQConfiger().getQDialect());
	
		Long count=0L;

		CombinedQueryExpr expr = exprUtil.matchAny();
		expr
		.addExpr(exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_EQ,"100"))
		.addExpr(exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_EQ,"300"));
		
		count=userDao.count(qbuilder.buildJpaSpecification(User.class, expr));
		System.out.println("query for:"+expr.toString()+" => "+ count);
		assertEquals(true ,count == 2L);
		
		
		expr = exprUtil.matchAny();
		expr
		.addExpr(exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_EQ,"100"))
		.addExpr(exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_EQ,"300"))
		.addExpr(exprUtil.buildSQE ("name",QueryPredicate.QP_EQ,preCreatedUser1));
		
		count=userDao.count(qbuilder.buildJpaSpecification(User.class, expr));
		System.out.println("query for:"+expr.toString()+" => "+ count);
		assertEquals(true ,count == 2L);
		
		
		expr = exprUtil.matchAny();
		expr
		.addExpr(exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_GTE,"100"))
		.addExpr(exprUtil.buildSQE ("cardNumber",QueryPredicate.QP_LTE,"300"))
		.addExpr(exprUtil.buildSQE ("name",QueryPredicate.QP_EQ,"xxx"));
		
		count=userDao.count(qbuilder.buildJpaSpecification(User.class, expr));
		System.out.println("query for:"+expr.toString()+" => "+ count);
		assertEquals(true ,count == 3L);
		
		deleteTestData();

	}
}

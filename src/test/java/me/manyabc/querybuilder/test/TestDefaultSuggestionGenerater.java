package me.manyabc.querybuilder.test;

import org.junit.Test;

import me.manyabc.querybuilder.client.model.FieldSuggestion;
import me.manyabc.querybuilder.core.QSuggestionGenerater;
import me.manyabc.querybuilder.core.impl.DefaultConfiger;
import me.manyabc.querybuilder.core.impl.DefaultConfigerChs;
import me.manyabc.querybuilder.core.impl.DefaultSuggestionGenerater;
import me.manyabc.querybuilder.exceptions.QueryBuilderException;
import me.manyabc.querybuilder.test.vo.UserVo;
import me.manyabc.querybuilder.utils.Printer;

import static org.junit.Assert.*;

import java.util.List;

public class TestDefaultSuggestionGenerater {

	@Test
	public void shouldGenerateSuggestions(){
		QSuggestionGenerater qg= new DefaultSuggestionGenerater(new DefaultConfiger());
		try {
			List<FieldSuggestion> result=qg.getFieldSuggestions(UserVo.class);
			assertEquals("should not return null ",result!= null ,true );
			assertEquals("should not return empty ",result.isEmpty() ,false);
			assertEquals("should contain 3 field ",result.size() ,3 );
			for(FieldSuggestion fs:result){
				//
				System.out.println(Printer.prints(fs)); 
			}
		} catch (QueryBuilderException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void shouldGenerateSuggestions2(){
		QSuggestionGenerater qg= new DefaultSuggestionGenerater(new DefaultConfigerChs());
		try {
			List<FieldSuggestion> result=qg.getFieldSuggestions(UserVo.class);
			assertEquals("should not return null ",result!= null ,true );
			assertEquals("should not return empty ",result.isEmpty() ,false);
			assertEquals("should contain 3 field ",result.size() ,3 );
			for(FieldSuggestion fs:result){
				//
				System.out.println(Printer.prints(fs)); 
			}
		} catch (QueryBuilderException e) {
			e.printStackTrace();
		}
	}
}

package me.manyabc.querybuilder.test.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import me.manyabc.querybuilder.core.QConfiger;
import me.manyabc.querybuilder.core.impl.DefaultConfigerChs;
import me.manyabc.querybuilder.test.service.MyService;
import me.manyabc.querybuilder.test.service.MyServiceImpl;

@Configuration
@ComponentScan(basePackages = {
    "me.manyabc.querybuilder.test"
})
public class TestConfig {

    @Bean
    public MyService getMyService() {
        return new MyServiceImpl();
    }

    @Bean(name="qconfiger")
    public QConfiger getQConfiger() {
        return new  DefaultConfigerChs();
    }

}

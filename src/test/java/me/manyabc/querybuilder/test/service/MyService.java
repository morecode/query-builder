package me.manyabc.querybuilder.test.service;

import java.util.List;

import me.manyabc.querybuilder.client.model.CombinedQueryExpr;
import me.manyabc.querybuilder.test.entity.User;


public interface MyService {
	List<User> queryList(CombinedQueryExpr exprs);
}

package me.manyabc.querybuilder.test.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity(name="T_USER")
public class User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6952710588037750116L;

	private Long id;

	private String name;
	private Date joinDate= new Date();
	private int	cardNumber;
	//private List<String> emails;
	
	private String email;
	private String phone;
	private String address;
	private String password;
	private String type ="ROLE_USER";
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column (name="FID")
	public Long getId() {
		return id;
	}
	/**
	 * 设置实体唯一标志
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	

	/**
	 * 获取用户唯一识别标志
	 * @return
	 */
	@Column(name="F_IDENT")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see 获取Email
	 * @return
	 */
	@Column(name="F_JOIN_DATE",nullable=true)
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	/**
	 * @see 获取Email
	 * @return
	 */
	@Column(name="F_CARD_NUMBER")
	public int getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(int cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @see 获取Email
	 * @return
	 */
	@Column(name="F_EMAIL",nullable=true)
	public String getEmail() {
		return email;
	}
	/**
	 * @see 设置Email
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @see 获取电话
	 * @return
	 */
	@Column(name="F_PHONE",nullable=true)
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @see 获取地址
	 * @return
	 */
	@Column(name="F_ADDRESS",nullable=true,length=128)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name="F_PASSWORD")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Column(name="F_TYPE")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}

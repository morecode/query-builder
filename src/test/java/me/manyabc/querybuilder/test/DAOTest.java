package me.manyabc.querybuilder.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import me.manyabc.querybuilder.test.config.JpaTestConfig;
import me.manyabc.querybuilder.test.config.TestConfig;
import me.manyabc.querybuilder.test.dao.UserDao;
import me.manyabc.querybuilder.test.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JpaTestConfig.class, TestConfig.class })
public class DAOTest {

	@Autowired
	UserDao userDao;

	public static User createTestUser(String userName) {

		User user = new User();
		user.setAddress("");
		user.setCardNumber(1);
		user.setEmail("email");
		user.setName(userName);
		user.setPassword("1234");
		user.setPhone("110");
		user.setType("test");
		return user;
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testUserDao() {


	}

}
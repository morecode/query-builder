package me.manyabc.querybuilder.test.vo;

import java.util.Date;
import java.util.List;

import me.manyabc.querybuilder.core.annotations.QSuggestion;

public class UserVo {
	@QSuggestion(describtion="用户名")
	private String name;
	@QSuggestion(describtion="注册时间")
	private Date joinDate;
	@QSuggestion(describtion="卡号")
	private int	cardNumber;
	private List<String> emails;
	
	
	public UserVo(){
		//
	}
	public UserVo(String name,Date joinDate,int	cardNumber,List<String> emails){
		this.name=name;
		this.joinDate=joinDate;
		this.cardNumber=cardNumber;
		this.emails=emails;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	public int getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(int cardNumber) {
		this.cardNumber = cardNumber;
	}
	public List<String> getEmails() {
		return emails;
	}
	public void setEmails(List<String> emails) {
		this.emails = emails;
	}
	
	
}
